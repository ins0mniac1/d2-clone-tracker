var intervalInSeconds = 60;
var intervalId
var dataFromApi = [];
var wrapper = document.querySelector('#app');
var audio = new Audio('../assets/beep.wav');

var lastUpdatedElement = {};

var oldData = []

const regions = {
    1: 'America',
    2: 'Europe',
    3: 'Asia'
};

const ladder = {
    1: 'Ladder',
    2: 'Non-Ladder',
};

const hc = {
    1: 'Hardcore',
    2: 'Softcore',
};

const intervalSelect = document.querySelector('#time-select');
const corsFreeURL = 'https://api.allorigins.win/get?charset=ISO-8859-1&url='
const apiURL = 'https://diablo2.io/dclone_api.php'

startLookup()
showData()
intervalSelect.addEventListener('change', (e) => {
    intervalInSeconds = e.target.value
    startLookup()

});

function startLookup() {


    if (intervalId) {
        stopLookup()
    }

    intervalId = setInterval(checkApi, (1000 * parseInt(intervalInSeconds)))
}

function stopLookup() {
    clearInterval(intervalId)
}

function checkApi() {
    axios.get(corsFreeURL + apiURL, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then((response) => {
            str = response.data.contents
            let obj = str.slice(
                str.indexOf('[') + 1,
                str.lastIndexOf(']'),
            )
            obj.split('},').map((el) => {
                if (!el.includes('}')) {
                    dataFromApi.push(JSON.parse(el + '}'))
                } else {
                    dataFromApi.push(JSON.parse(el))
                }
            })

            showData()
        })
}

function showData() {
    while (wrapper.firstChild) {
        wrapper.removeChild(wrapper.lastChild);
    }
    dataFromApi
        .sort((a, b) => (a.progress > b.progress) ? 1 : -1)
        .sort((a, b) => (a.hc > b.hc) ? 1 : -1)
        .sort((a, b) => (a.ladder > b.ladder) ? 1 : -1)
        .map((element) => {
            let index = dataFromApi.map(i => i.timestamped).indexOf(element.timestamped)
            let isSameAsOldElement = compare(index, element)
            createHTMLElement(element, ! isSameAsOldElement)
        })

    if (lastUpdatedElement.region) {
        showLastUpdatedElement(lastUpdatedElement)
    }

    oldData = dataFromApi
    dataFromApi = []
}

function compare(index, element) {
    if (! oldData.length) {
        return true;
    }

    let elementToCompare = oldData
        .sort((a, b) => (a.progress > b.progress) ? 1 : -1)
        .sort((a, b) => (a.hc > b.hc) ? 1 : -1)
        .sort((a, b) => (a.ladder > b.ladder) ? 1 : -1)[index]
    return elementToCompare.progress === element.progress;
}

function createHTMLElement(data, alert) {

    let progressSpan = document.createElement('span')
    progressSpan.innerText = 'Progress: '
    let progressStrong = document.createElement('strong')
    progressStrong.innerText = data.progress + ' / 6'

    let regionSpan = document.createElement('span')
    regionSpan.innerText = 'Region: '
    let regionStrong = document.createElement('strong')
    regionStrong.innerText = readProperty(regions, data.region)

    let ladderSpan = document.createElement('span')
    ladderSpan.innerText = 'Ladder: '
    let ladderStrong = document.createElement('strong')
    ladderStrong.innerText = readProperty(ladder, data.ladder)

    let modeSpan = document.createElement('span')
    modeSpan.innerText = 'Mode: '
    let modeStrong = document.createElement('strong')
    modeStrong.innerText = readProperty(hc, data.hc)

    let timeSpan = document.createElement('span')
    timeSpan.innerText = 'Time: '
    let timeStrong = document.createElement('strong')
    timeStrong.innerText = getHumanDate(data.timestamped)

    let progressDiv = document.createElement('div')
    progressDiv.appendChild(progressSpan)
    progressDiv.appendChild(progressStrong)

    let regionDiv = document.createElement('div')
    regionDiv.appendChild(regionSpan)
    regionDiv.appendChild(regionStrong)

    let ladderDiv = document.createElement('div')
    ladderDiv.appendChild(ladderSpan)
    ladderDiv.appendChild(ladderStrong)

    let modeDiv = document.createElement('div')
    modeDiv.appendChild(modeSpan)
    modeDiv.appendChild(modeStrong)

    let timeDiv = document.createElement('div')
    timeDiv.appendChild(timeSpan)
    timeDiv.appendChild(timeStrong)

    let progressWrapper = document.createElement('div')
    progressWrapper.classList.add('progress-wrapper')
    if (alert) {
        progressWrapper.classList.add('alert')
        audio.play();
        lastUpdatedElement = data
    }
    progressWrapper.appendChild(progressDiv)
    progressWrapper.appendChild(regionDiv)
    progressWrapper.appendChild(ladderDiv)
    progressWrapper.appendChild(modeDiv)
    progressWrapper.appendChild(timeDiv)

    wrapper.appendChild(progressWrapper)
}

function getHumanDate(timestamp) {
    let date = new Date(timestamp * 1000);

    let hours = date.getHours();

    let minutes = "0" + date.getMinutes();

    let seconds = "0" + date.getSeconds();

    return  hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
}

function readProperty(obj, prop) {
    return obj[prop];
}

function showLastUpdatedElement(element){
    document.querySelector('#last-progress').innerHTML = element.progress + ' / 6'
    document.querySelector('#last-region').innerHTML = readProperty(regions, element.region)
    document.querySelector('#last-ladder').innerHTML = readProperty(ladder, element.ladder)
    document.querySelector('#last-mode').innerHTML = readProperty(hc, element.hc)
    document.querySelector('#last-time').innerHTML = getHumanDate(element.timestamped)
}